'use strict';

/**
 * @ngdoc overview
 * @name mytodoApp
 * @description
 * # mytodoApp
 *
 * Main module of the application.
 */
angular
  .module('mytodoApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.sortable',
    'checklist-model',
    'fundoo.services'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/requests', {
        templateUrl: 'views/requests/requests.html',
        controller: 'RequestsCtrl'
      })
      .when('/new-request/:remedy_id?/:capture_step?', {
       /* templateUrl: function (params) {
          var capture_step = (params.capture_step) ? params.capture_step : 'general_data';
          return 'views/requests/'+ capture_step +'.html'; 
        },
        */
        templateUrl: 'views/requests/new_request.html',
        controller: 'RequestCreateCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
