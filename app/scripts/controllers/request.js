'use strict';

/**
 * @ngdoc function
 * @name mytodoApp.controller:RequestCtrl
 * @description
 * # RequestCtrl
 * Controller of the mytodoApp
 */
angular.module('mytodoApp')

  // ---  RequestsCtrl -- //
  .controller('RequestsCtrl', function ($scope, Requests) {
    this.getRequests = function() {
      Requests.getRequestsPromise().then(function(data) {
        //console.log(JSON.stringify(data));
        $scope.requests = data;
      }, function(err) {
        console.log("error: controller: " + err)
      })
    };
    this.getRequests();
  })

  // ---  RequestCreateCtrl -- //
  .controller('RequestCreateCtrl', function ($scope, $location, $routeParams, Requests, CaptureSteps, createDialog) {
    var remedy_id = ($routeParams.remedy_id) ? parseInt($routeParams.remedy_id) : 0;
    var capture_step = ($routeParams.capture_step) ? $routeParams.capture_step : 'general_data';

    // Capture Steps Class
    var stepsObj = {};
    stepsObj.getCaptureSteps = function() {
      CaptureSteps.getCaptureStepsPromise().then(function(captureSteps) {
        // Current step
        $scope.CaptureSteps = captureSteps;
        $scope.currentCaptureStepLabel = captureSteps[capture_step];

        // Steps history
        var completedSteps = {};
        for (var slugCaptureStep in $scope.form.Request.CaptureStep)
        {
           completedSteps[slugCaptureStep] = {
            slug: slugCaptureStep,
            label: captureSteps[slugCaptureStep],
            status: $scope.form.Request.CaptureStep[slugCaptureStep],
            current: (capture_step == slugCaptureStep) ? true: false,
            target: 'views/partials/request/_'+slugCaptureStep+'.html',
            href: '/new-request/'+ remedy_id +'/' + slugCaptureStep
           };
        }
        $scope.completedSteps = completedSteps;
        //console.log($scope.completedSteps)

        // Steps' flow
        var stepsFlow = {};
        var currrentStep = null;
        var keys = Object.keys(completedSteps);
        for(var i = 0; i < keys.length; i++)
        {
            if(keys[i] == capture_step)
            {
              currrentStep = i;
            }
        }
        if(currrentStep != 0) stepsFlow.previous = '/new-request/'+ remedy_id +'/' + keys[currrentStep-1];
        if(keys[currrentStep+1] != undefined) stepsFlow.next = '/new-request/'+ remedy_id +'/' + keys[currrentStep+1];
        $scope.stepsFlow = stepsFlow;
        //console.log(stepsFlow);

      }, function(err) {
        console.log("error: controller: " + err)
      })
    };
    
    // Request Data
    if(remedy_id != 0)
    {
        this.getRequest = function(stepsObj) {
          Requests.getRequestPromise(remedy_id).then(function(data) {
            
            // Adding a new capture step 
            if(data.Request.CaptureStep[capture_step] == undefined)
            {
              data.Request.CaptureStep[capture_step] = 'Pending';
            }

            // Main data
            $scope.form = data;
            stepsObj.getCaptureSteps();
            //console.log(JSON.stringify(data));

            if(capture_step == 'management_type')
            {
              // $scope.form.Request.management_type = '';
              // TODO - management_type service
            }
            else if(capture_step == 'infraestructure')
            {
              $scope.infrastructureResources = [
                {slug: 'so', label: 'Sistema Opertivos'},
                {slug: 'apps', label: 'Aplicativos'},
                {slug: 'dbs', label: 'Bases de datos'},
                {slug: 'data-devices', label: 'Equipos de datos (Ruteadores y Firewalls'},
                {slug: 'plataform-interactions', label: 'Interacción con otras plataformas'}
              ];
            }
            else if(capture_step == 'so')
            {
              $scope.addButton = {
                action: "modifyServerBox('"+data.Request.remedy_id+"', undefined)"
              };
            }
          }, function(err) {
            console.log("error: controller: " + err)
          })
        };
        this.getRequest(stepsObj);
    }
    else
    {
      $scope.form = {
        Request: {
          CaptureStep: {
            general_data: 'Pending'
          },
          remedy_id: '',
          system_name: '',
          delivery_charge : 'Areli Octavio Solis Espitia' // TODO - session (logged user's name)
        }
      };
      stepsObj.getCaptureSteps();
    }

    $scope.remedy_id = remedy_id;
    $scope.capture_step = capture_step;
    $scope.redirectToIndex = false;

    // Modify Request action
    $scope.modifyRequest = function () {
      this.saveRequest = function() {
        $scope.form.Request.CaptureStep[capture_step] = 'Completed';
        Requests.saveRequestPromise($scope.form).then(function(data) {
          //console.log(JSON.stringify(data.Request.remedy_id));
          //console.log(JSON.stringify(data.next_capture_step));
          var redirect = ($scope.redirectToIndex == true)
            ? '/requests' 
            : '/new-request/'+ data.Request.remedy_id +'/' + data.next_capture_step;
          $location.path(redirect);
        }, function(err) {
          console.log(err);
          if(err.form_errors != undefined)
          {
            $scope.form_errors = err.form_errors;
          }
        })
      };
      this.saveRequest();
    };

    // Redirect to requests index
    $scope.redirectIndex = function () {
      $scope.redirectToIndex = true;
    };

    // Capture process redirection
    $scope.redirectToStep = function (href) {
      $location.path(href);
    };

    // Request to modify server in BackEnd
    $scope.modifyServer = function () {
      this.saveRequest = function() {
        Requests.saveRequestPromise($scope.form).then(function(data) {
          //console.log(JSON.stringify(data.Request.remedy_id));
          //console.log(JSON.stringify(data.next_capture_step));
          var redirect = ($scope.redirectToIndex == true)
            ? '/requests' 
            : '/new-request/'+ data.Request.remedy_id +'/' + data.next_capture_step;
          $location.path(redirect);
        }, function(err) {
          console.log("error: controller: " + err)
        })
      };
      this.saveRequest();
    };

    // Dialog box to Edit Servers
    $scope.modifyServerBox = function(remedy_id, server_id) {
      createDialog('views/requests/new_server.html', {
        id: 'DialogBox',
        title: 'Create Server',
        backdrop: true,
        success: {
          label: 'Save', 
          fn: function() {
            // Preparing json for servers
            var scope = angular.element('#RequestHasServerForm').scope();
            scope.form.Request.Servers = [];
            
            // New server
            if(scope.form.Server._id == undefined && $scope.form.Request.Servers != undefined)
            {
              scope.form.Request.Servers = $scope.form.Request.Servers;
              scope.form.Request.Servers.push(scope.form.Server);
            }
            // Existing Server
            else
            {
              for (var server in $scope.form.Request.Servers)
              {
                var edited = $scope.form.Request.Servers[server]._id == scope.form.Server._id;
                scope.form.Request.Servers.push((edited ? scope.form.Server: $scope.form.Request.Servers[server]));
              }
            }

            $scope.form.Request = scope.form.Request;
            //console.log($scope.form.Request);
            $scope.modifyServer();
          }
        },
        controller: 'RequestModifyServerCtrl',
        css: {
            width: '900px',
            margin: 'auto'
        }
      }, {
        remedy_id: remedy_id, 
        server_id: server_id
      });
    };

    // Delete Servers
    $scope.deleteServerBox = function(remedy_id, server_id) {
      createDialog('views/requests/delete_server.html', {
        id: 'DialogBox',
        title: 'Delete Server',
        backdrop: true,
        success: {
          label: 'Save', 
          fn: function() {
            var scope = angular.element('#RequestHasServerForm').scope();

            scope.form.Request.Servers = [];
            for (var server in $scope.form.Request.Servers)
            {
              var deleted = $scope.form.Request.Servers[server]._id == scope.form.Server._id;
              if(!deleted)
              {
                 scope.form.Request.Servers.push($scope.form.Request.Servers[server]);
              }
            }

            $scope.form.Request = scope.form.Request;
            //console.log($scope.form.Request);
            $scope.modifyServer();
          }
        },
        controller: 'RequestModifyServerCtrl',
        css: {
            width: '600px',
            margin: 'auto'
        }
      }, {
        remedy_id: remedy_id, 
        server_id: server_id
      });
    };
  })

  // Managing Servers
  // ---  RequestModifyServerCtrl -- //
  .controller('RequestModifyServerCtrl', function ($scope, Requests, remedy_id, server_id) {
    this.getRequestToEdit = function() {
      Requests.getRequestPromise(remedy_id).then(function(data) {
        $scope.form = {
          Request: {
            id: data.Request.id,
            remedy_id: data.Request.remedy_id
          }
        }
        
        if(data.Request.Servers == undefined)
        {
          $scope.form.Server = {
            ip_address: '',
            hostname: ''
          };
        }
        else
        {
          for (var server in data.Request.Servers)
          {
            if(data.Request.Servers[server]._id == server_id)
            {
              $scope.server_id = server_id;
              $scope.form.Server = data.Request.Servers[server];
            }
          }
        }

        $scope.operativeSystems = {
          'Windows Server' : {
            items: ['Windows 1', 'Windows 2']
          },
          'Linux' : {
            distributions: {
              'distribution 1': ['Linux 1', 'Linux 2'],
              'distribution 2': ['Linux 3', 'Linux 4'],
            }
          },
          'Otros' : {
            items: ['Otros 1', 'Otros 2']
          },
        };

        // Operative Systems dependency on comboboxes
        $scope.updateOSCombos = function () {
          if($scope.form.Server == undefined)
          {
            $scope.form.Server = [];
          }

          if(!($scope.form.Server.operative_system == '' || $scope.form.Server.operative_system == undefined))
          {
            if($scope.form.Server.operative_system == 'Windows Server')
            {
              $scope.access_types = ['FTP', 'RDP'];
            }
            else
            {
              $scope.access_types = ['FTP', 'SSH'];
            }

            var os = $scope.operativeSystems[$scope.form.Server.operative_system];
            os.distributions != undefined ? $scope.distributions = os.distributions: delete $scope.distributions; 

            if(os.distributions != undefined && !($scope.form.Server.distribution == '' || $scope.form.Server.distribution == undefined))
            {           
              os.distributions[$scope.form.Server.distribution] != undefined 
                ? $scope.versions = os.distributions[$scope.form.Server.distribution]
                : delete $scope.versions;
            }
            else if(os.items != undefined)
            {
              delete $scope.form.Server.distribution;
              $scope.versions = os.items;
            }
          }
          else
          {
             $scope.access_types = [];
          }
        };
        $scope.updateOSCombos();

        // Managing users' functions
        $scope.addNewUser = function() {
          if($scope.form.Server.Users == undefined)
          {
            $scope.form.Server.Users = [];
          }
          $scope.form.Server.Users.push({});
        };

        // Delete users
        $scope.removeUser = function(hashKey) {
          for (var index = 0; index < $scope.form.Server.Users.length; index++)
          {
              if(hashKey == $scope.form.Server.Users[index].$$hashKey)
              {
                $scope.form.Server.Users.splice(index, 1);
                return true;
              }
          }
        };

        // Set default port for services
        $scope.updatePort = function(index) {
          var access_type = $scope.form.Server.Users[index].access_type;
          if(access_type == 'SSH')
          {
            $scope.form.Server.Users[index].port = 22;
          }
          else if(access_type == 'FTP')
          {
            $scope.form.Server.Users[index].port = 21;
          }
          else if(access_type == 'RDP')
          {
            $scope.form.Server.Users[index].port = 3389;
          }
          else
          {
            $scope.form.Server.Users[index].port = '';
          }
        };

      }, function(err) {
        console.log("error: controller: " + err)
      })
    };
    this.getRequestToEdit();
  });
