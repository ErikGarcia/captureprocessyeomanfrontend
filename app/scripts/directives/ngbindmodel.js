'use strict';

/**
 * @ngdoc directive
 * @name mytodoApp.directive:ngBindModel
 * @description
 * # ngBindModel
 */
angular.module('mytodoApp')
  .directive('ngBindModel', function ($compile) {
    return {
      link:function(scope,element,attr){
          element[0].removeAttribute('ng-bind-model');
          element[0].setAttribute('ng-model',scope.$eval(attr.ngBindModel));
          $compile(element[0])(scope);
        }
    };
  });
