'use strict';

/**
 * @ngdoc service
 * @name mytodoApp.CaptureSteps
 * @description
 * # CaptureSteps
 * Service in the mytodoApp.
 */
angular.module('mytodoApp')
  .service('CaptureSteps', function ($http, $q) {
    this.getCaptureStepsPromise = function() { 
      var defer = $q.defer();
      $http.get('http://localhost/cake-mongo-rest/capture_steps')
        .success(function(data) {             
            defer.resolve(data);
          })
        .error(function(err, status) {
            defer.reject(err);
          });
      return defer.promise;
    }
  });
