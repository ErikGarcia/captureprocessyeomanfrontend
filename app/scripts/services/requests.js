'use strict';

/**
 * @ngdoc service
 * @name mytodoApp.Requests
 * @description
 * # Requests
 * Service in the mytodoApp.
 */
angular.module('mytodoApp')
  .service('Requests', function ($http, $q) {
    
    this.getRequestsPromise = function() { 
      var defer = $q.defer();
      $http.get('http://localhost/cake-mongo-rest/requests')
        .success(function(data) {             
            defer.resolve(data);
          })
        .error(function(err, status) {
            defer.reject(err);
          });
      return defer.promise;
    }

    this.getRequestPromise = function(remedy_id) { 
      var defer = $q.defer();
      $http.get('http://localhost/cake-mongo-rest/requests/view/'+remedy_id)
        .success(function(data) {             
            defer.resolve(data);
          })
        .error(function(err, status) {
            defer.reject(err);
          });
      return defer.promise;
    }

    this.saveRequestPromise = function (request) {
      var defer = $q.defer();
      $http.post('http://localhost/cake-mongo-rest/requests/add', request)
        .success(function(data) {             
            defer.resolve(data);
          })
        .error(function(err, status) {
            defer.reject(status == 400 ? { form_errors: err }: err);
          });
      return defer.promise;
  	};

  });
