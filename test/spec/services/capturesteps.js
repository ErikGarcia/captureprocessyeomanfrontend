'use strict';

describe('Service: CaptureSteps', function () {

  // load the service's module
  beforeEach(module('mytodoApp'));

  // instantiate service
  var CaptureSteps;
  beforeEach(inject(function (_CaptureSteps_) {
    CaptureSteps = _CaptureSteps_;
  }));

  it('should do something', function () {
    expect(!!CaptureSteps).toBe(true);
  });

});
