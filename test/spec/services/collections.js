'use strict';

describe('Service: Collections', function () {

  // load the service's module
  beforeEach(module('mytodoApp'));

  // instantiate service
  var Collections;
  beforeEach(inject(function (_Collections_) {
    Collections = _Collections_;
  }));

  it('should do something', function () {
    expect(!!Collections).toBe(true);
  });

});
