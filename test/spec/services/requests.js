'use strict';

describe('Service: Requests', function () {

  // load the service's module
  beforeEach(module('mytodoApp'));

  // instantiate service
  var Requests;
  beforeEach(inject(function (_Requests_) {
    Requests = _Requests_;
  }));

  it('should do something', function () {
    expect(!!Requests).toBe(true);
  });

});
